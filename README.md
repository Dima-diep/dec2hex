# hex2dec

## What is it?

It's converter of numbers from DEC to HEX. Usage:
```
 ~ $ ./hex2dec 10
a
 ~ $ ./hex2dec 15 16
f 10
```

## Compilation

There is no need any specific libraries, only C++ (cross)compiler.
```
 ~ # apt install g++
 ~ # g++ dec2hex.cpp -o dec2hex
 ~ # ./hex2dec 838 1706
dc af
```
